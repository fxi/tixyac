import { Visualiser } from './vis.js';
import { onNextFrame } from './animation_frame.js';
import Tweakpane from 'tweakpane';
import copy from 'copy-text-to-clipboard';

const vis = new Visualiser('draw', {
  restore: true,
});

let tStart = performance.now();

render();

window.vis = vis;

function render() {
  const t = performance.now() - tStart;
  vis.draw(t);
  onNextFrame(render);
}

const elInput = document.getElementById('code');
elInput.value = vis.getCode();
elInput.addEventListener('input', (e) => {
  vis.setCode(e.target.value);
});

/**
* Window change
*/
window.addEventListener("resize", ()=>{
  vis.setSize();
});


/**
 * TweakPane
 */
const pane = new Tweakpane({
  title: 'Parameters',
});
const btnSave = pane.addButton({
  title: 'Save',
});
btnSave.on('click', vis.urlSave);
const btnClipBoard = pane.addButton({
  title: 'Copy to clipboard',
});
btnClipBoard.on('click', () => {
  const url = vis.urlSave();
  if (url) {
    copy(url.toString());
  }
});

const stateBack = vis.config.state.back;
const stateGrid = vis.config.state.grid;
const stateMain = vis.config.state.main;

const grid = pane.addFolder({ title: 'Grid' });

grid.addInput(stateGrid, 'colorA');
grid.addInput(stateGrid, 'colorB');
grid.addInput(stateGrid, 'nX', {
  step: 1,
  min: 1,
  max: 50,
});
grid.addInput(stateGrid, 'nY', {
  step: 1,
  min: 1,
  max: 50,
});
grid.addInput(stateGrid, 'radius', {
  step: 1,
  min: 0,
  max: 40,
});
grid.addInput(stateGrid, 'offset', {
  step: 1,
  min: 0,
  max: 100,
});

const back = pane.addFolder({ title: 'Accumulator' });
back.addInput(stateBack, 'color');
back.addInput(stateBack, 'alpha', {
  step: 0.01,
  min: 0,
  max: 0.5,
});
back.addInput(stateBack, 'gravity', {
  step: 0.1,
  min: -10,
  max: 10,
});
back.addInput(stateBack, 'rotate', {
  step: 0.001,
  min: -0.05,
  max: 0.05,
});
back.addInput(stateBack, 'scale', {
  step: 0.01,
  min: 0.9,
  max: 1.1,
});
pane.addInput(stateMain, 'offset', {
  x: { min: -vis.width / 2, max: vis.width / 2 },
  y: { min: -vis.height / 2, max: vis.height / 2 },
});
pane.addMonitor(vis, 'value', {
  count: 5,
});
