/* jshint esversion:8 */

const defaults = {
  width: null,
  height: null,
  restore: false,
  state: {
    back: {
      rotate: 0.005,
      color: { r: 70, g: 70, b: 70 },
      alpha: 0.01,
      gravity: 0.7,
      scale: 0.99,
    },
    grid: {
      colorA: { r: 255, g: 0, b: 255 },
      colorB: { r: 0, g: 0, b: 0 },
      radius: 20,
      offset: 40,
      nX: 16,
      nY: 16,
    },
    main: {
      code: '((x^y)%2?sin:cos)(t)',
      code_default: '((x^y)%2?sin:cos)(t)',
      offset: {
        x: 0,
        y: 0,
      },
    },
  },
};

class Visualiser {
  constructor(id, config) {
    const vis = this;
    vis.config = Object.assign({}, defaults, config);
    vis.elCanvas = document.getElementById(id);
    if (!vis.elCanvas) {
      throw new Error('no canvas');
    }
    if (config.restore) {
      vis.urlRestore();
    }
    vis.init();
    return vis;
  }
  init() {
    const vis = this;
    const config = vis.config;
    vis.elCanvasBack = document.createElement('canvas');
    vis.width = config.with || window.innerWidth;
    vis.height = config.height || window.innerHeight;
    vis.i = 0;
    vis.ctx = vis.elCanvas.getContext('2d');
    vis.ctxBack = vis.elCanvasBack.getContext('2d');
    vis.setSize();
    vis.clear();
    vis.setCode();
    /**
     * binds. mmh ?
     */
    vis.draw = vis._draw.bind(vis);
    vis.urlSave = vis.urlSave.bind(vis);
    vis.urlRestore = vis.urlRestore.bind(vis);
  }

  urlSave(e) {
    const vis = this;
    if (e instanceof Event) {
      e.preventDefault();
    }
    const state = vis.getState();
    const url = new URL(document.location);
    url.searchParams.set('state', JSON.stringify(state));
    history.replaceState(null, null, url);
    return url;
  }

  urlRestore(e) {
    const vis = this;
    if (e instanceof Event) {
      e.preventDefault();
    }
    const url = new URL(document.location);
    if (url.searchParams.has('state')) {
      try {
        const stateToRestore = JSON.parse(url.searchParams.get('state'));
        vis.setState(stateToRestore);
      } catch (err) {
        console.warn('Issue when restoring state', err);
      }
    }
  }

  getState() {
    return this.config.state;
  }

  setState(state) {
    const vis = this;
    const origState = vis.config.state;
    state = Object.assign({}, origState, state);
    for (let k in state) {
      state[k] = Object.assign({}, origState[k], state[k]);
    }
    vis.config.state = state;
  }

  setWidth(w) {
    const vis = this;
    vis.width = w || vis.elCanvas.width;
    vis.elCanvas.width = vis.width;
    vis.elCanvasBack.width = vis.width;
    vis.updateCenter();
  }

  setHeight(h) {
    const vis = this;
    vis.height = h || vis.elCanvas.height;
    vis.elCanvas.height = vis.height;
    vis.elCanvasBack.height = vis.height;
    vis.updateCenter();
  }

  setSize(opt) {
    const vis = this;
    const rect = vis.elCanvas.parentElement.getBoundingClientRect();
    const o = Object.assign({}, { w: rect.width, h: rect.height }, opt);
    vis.setWidth(o.w);
    vis.setHeight(o.h);
  }
  
  clear() {
    const vis = this;
    vis.ctx.setTransform(1, 0, 0, 1, 0, 0);
    vis.ctx.fillStyle = 'black';
    vis.ctx.fillRect(0, 0, vis.width, vis.height);
  }

  updateCenter() {
    const vis = this;
    vis.center = {
      x: vis.width / 2,
      y: vis.height / 2,
    };
  }

  /**
   * Draw background effect
   */
  drawBackground() {
    const vis = this;
    const oM = vis.config.state.main;
    const oB = vis.config.state.back;

    const x = vis.center.x + oM.offset.x;
    const y = vis.center.y + oM.offset.y;
    /*
     * reset matrix transform
     */
    vis.ctxBack.setTransform(1, 0, 0, 1, 0, 0);

    /*
     * set center, scale and rotate
     */
    vis.ctxBack.translate(x, y);
    vis.ctxBack.rotate(oB.rotate);
    vis.ctxBack.scale(oB.scale, oB.scale);
    vis.ctxBack.translate(0, oB.gravity);
    vis.ctxBack.translate(-x, -y);

    /*
     * draw current canvas on background
     */
    vis.ctxBack.drawImage(vis.elCanvas, 0, 0);

    /*
     * Add fading
     */
    vis.ctxBack.fillStyle = hex2rgba(oB.color, oB.alpha);
    vis.ctxBack.fillRect(0, 0, vis.width, vis.height);

    /*
     * Draw resulting background in main canvas
     */
    vis.ctx.drawImage(vis.elCanvasBack, 0, 0);
  }

  /**
   * Draw
   */

  _draw(t) {
    const vis = this;
    vis.t = t;
    vis.i++;
    vis.drawGrid();
    vis.drawBackground();
  }

  getCode() {
    return this.config.state.main.code;
  }

  setCode(txt) {
    const vis = this;
    const o = vis.config.state.main;

    try {
      if (txt) {
        o.code = txt;
      } else {
        o.code = o.code;
      }
      o.code = o.code.replace(/\\/g, ';');
      this._fun = new Function(
        't',
        'i',
        'x',
        'y',
        `
    let out = 0;
    try{
        with (Math) {
          return Number(${o.code});
        }
     }catch(e){};
    `,
      );
    } catch (e) {}
  }

  drawGrid() {
    const vis = this;
    const oM = vis.config.state.main;
    const oG = vis.config.state.grid;
    const rMax = oG.radius;
    const o = oG.offset;
    const nX = oG.nX;
    const nY = oG.nY;
    const cA = hex2rgba(oG.colorA, 1);
    const cB = hex2rgba(oG.colorB, 1);
    const origX = vis.center.x + oM.offset.x - ((nX - 1) / 2) * o;
    const origY = vis.center.y + oM.offset.y - ((nY - 1) / 2) * o;
    let x = 0;
    let y = 0;
    let i = 0;
    let v;
    for (x = 0; x < nX; x++) {
      for (y = 0; y < nY; y++) {
        v = vis._fun(vis.t / 1000, i++, x, y);
        let c = cA;
        let r = (v * rMax) / 2;
        if (r < 0) {
          r = -r;
          c = cB;
        }
        if (r > rMax / 2) {
          r = rMax / 2;
        }
        vis.value = r;
        vis.ctx.beginPath();
        vis._circle(origX + x * o, origY + y * o, r);
        vis.ctx.fillStyle = c;
        vis.ctx.fill();
      }
    }
  }

  _circle(x, y, r) {
    const vis = this;
    vis.ctx.arc(x, y, r, 0, 2 * Math.PI);
  }
}

// jshint ignore:end
function hex2rgba(hex, opacity) {
  const r = parseInt(hex.r, 16);
  const g = parseInt(hex.g, 16);
  const b = parseInt(hex.b, 16);
  return `rgba(${r},${g},${b},${opacity})`;
}

export { Visualiser };
