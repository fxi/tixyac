# Tixyac

Configurable clone of [aemkei/tixy](https://github.com/aemkei/tixy).

![alt text](./static/tixyac.gif "tixiac ui")


### Links

Original app: [tixy.land](https://tixy.land/).
Radial version: [100.antfu.me/005](https://100.antfu.me/005).

### Dev 
`npm run start`

### Prod
`npm run build`
